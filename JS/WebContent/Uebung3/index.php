<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tabelle mit CSS formatiert</title>
        <link href="style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <table class="tbl">
            <caption>Tabellenüberschrift</caption>
            <tr>
                <th>Spalte 1
                <th>Spalte 2
                <th>Spalte 3
                <th>Spalte 4
            </tr>
            <tr>
                <td> 1
                <td> 2
                <td> 3
                <td> 4
            </tr>
            <tr>
                <td> 5
                <td rowspan="2"> 6
                <td> 7
                <td> 8
            </tr>
            <tr>
                <td> 9
                <td> 10
                <td> 11
            </tr>
            <tr>
                <td> 12
                <td> 13
                <td> 14
                <td> 15
            </tr>
            <tr>
                <td> 16
                <td> 17
                <td> 18
                <td> 19
            </tr>
        </table>

        
        <!--Diese Tabelle soll nicht mit CSS formatiert werden!-->
        
        <table>
            <caption>Sonnensystem</caption>
            <tr>
                <th>Planet
                <th>Satelliten
            </tr>
            <tr>
                <td>Merkur
                <td>-
            </tr>
            <tr>
                <td>Venus
                <td>-
            </tr>
            <tr>
                <td>Erde
                <td>Mond
            </tr>
            <tr>
                <td>Mars
                <td>Phobos und Daimos
            </tr>
        </table>
        
    </body>
</html>
