/**
 * 
 */

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;

/**
 * Eine Engine f�r den einfachen Dateimanager. Diese Klasse macht die
 * tats�chliche Arbeit, die �ber den <code>FileManagerView</code> zug�nglich ist
 * 
 * @author dominik@gierczak.net
 * 
 */
public class FileManagerEngine implements IFileManager {

    private File current;

    /**
     * Konstruktor
     * 
     * @param start
     *            hier fangen wir an
     */
    public FileManagerEngine(File start) {
	if (start == null)
	    throw new NullPointerException("Pfad oder Datei nicht vorhanden.");
	current = start;
    }

    /*
     * (non-Javadoc)
     * 
     * @see IFileManager#operationEnter(java.io.File)
     */
    @Override
    public void operationEnter(File toEnter) {
	if (toEnter == null)
	    throw new NullPointerException("Pfad oder Datei nicht vorhanden.");

	if (toEnter.exists()) {
	    current = toEnter;
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see IFileManager#operationUp()
     */
    @Override
    public void operationUp() {
	if (current.getParentFile() != null) {
	    current = current.getParentFile();
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see IFileManager#operationGetRWX(java.io.File)
     */

    /*
     * Throws NoSuchFileException muss hier dringend ergaenzt werden, da sonst
     * die notwendige Fehlerbehandelung nicht funktioniert.
     */
    @Override
    public String operationGetRWX(File toGetRWX) throws NoSuchFileException {
	if (toGetRWX == null)
	    throw new NullPointerException("Datei oder Pfad nicht vorhanden.");

	if (!toGetRWX.exists())
	    throw new NoSuchFileException(toGetRWX.toString());

	return new StringBuilder().append(toGetRWX.canRead() ? "R" : "-")
		.append(toGetRWX.canWrite() ? "W" : "-")
		.append(toGetRWX.canExecute() ? "X" : "-").toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see IFileManager#operationCreateDir(java.lang.String)
     */
    @Override
    public boolean operationCreateDir(String name) {
	if (name == null) {
	    return false;
	} else {
	    return new File(current.getAbsolutePath() + name).mkdir();
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see IFileManager#operationCopyFile(int, java.lang.String)
     */
    @Override
    public boolean operationCopyFile(int source, String dest) {
	// Aus Effizenzgruenden werden Exceptions asap ausgeloest
	if (outOfRange(source))
	    throw new IndexOutOfBoundsException();

	if (current.listFiles()[source].isDirectory()) {
	    return false;
	}

	/*
	 * Moeglichkeit A
	 */
	// if (dest == null) return false;

	/*
	 * Moeglichkeit B wird bevorzugt, da der Grund fuer den Fehler angegeben
	 * wird.
	 */
	if (dest == null)
	    throw new NullPointerException("Dateiname unzulaessig!");

	try {
	    copyFile(current.listFiles()[source], new File(dest));
	} catch (IOException e) {
	    return false;
	}

	// Kopiervorgang erfolgreich abgeschlossen
	return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see IFileManager#getCurrentPath()
     */
    @Override
    public String getCurrentPath() throws IOException {
	return current.getCanonicalPath();
    }

    /*
     * (non-Javadoc)
     * 
     * @see IFileManager#getContentAsString()
     */
    @Override
    public String getContentAsString() {
	StringBuffer sb = new StringBuffer();
	// sb.append(String.format("%2$10s (%1$s)", "Nummer, "Name"));
	for (int i = 0; i < current.list().length; i++) {
	    sb.append(String.format("%2$-30s(%1$d)%n", i + 1, current.list()[i]));
	}
	return sb.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see IFileManager#numberToFile(int)
     */
    @Override
    public File numberToFile(int i) {
	if (outOfRange(i))
	    throw new IndexOutOfBoundsException();
	return current.listFiles()[i];
    }

    private boolean outOfRange(int i) {
	return !rangeCheck(i);
    }

    /*
     * (non-Javadoc)
     * 
     * @see IFileManager#rangeCheck(int)
     */
    @Override
    public boolean rangeCheck(int i) {
	return (i > 0 && i <= current.listFiles().length);
    }

    private void copyFile(File source, File dest) throws IOException {
	// Ausnahmen werden weitergeleitet
	Files.copy(source.toPath(), new BufferedOutputStream(
		new FileOutputStream(dest)));

    }
}
