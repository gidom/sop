import java.io.*;
import java.nio.file.NoSuchFileException;

/**
 * Schnittstelle f�r einfache Filemanager. Diese Filemanager haben ein
 * "aktuelles Verzeichnis", in dem sie ihre Operationen durchf�hren. Die
 * Darstellung des aktuellen Verzeichnisses erfolgt in der Reihenfolge
 * <ol>
 * <li>Verzeichnisse</li>
 * <li>Dateien</li>
 * </ol>
 * und innerhalb dieser Gruppierung dann (ASCII-)alphabetisch. Um die Eintr�ge
 * (Dateien und Verzeichnisse) "ansprechbar" zu machen, soll jedem Eintrag eine
 * Nummer zugeordnet werden. Die Umrechnung von Nummer in Eintr�ge und zur�ck
 * obliegt der implementierenden Klasse. Die Umrechnung muss zur Verf�gung
 * gestellt werden, damit die f�r die Anzeige verantwortliche Klasse darauf
 * zur�ckgreifen kann.
 * <p>
 * Weiterhin muss die f�r die Anzeige verantwortliche Klasse die M�glichkeit
 * haben, eine eingegebene Nummer auf ihre G�ltigkeit pr�fen zu lassen.
 * </p>
 * Diese Dateimanager m�ssen nicht robust gegen�ber �nderungen am
 * Verzeichnisbaum sein, z.B. das aktuelle Verzeichnis wird durch ein anderes
 * Programm gel�scht.
 * 
 * @author hmueller
 */
public interface IFileManager {

    /**
     * Geht in das Verzeichnis toEnter. Ist <code>toEnter</code> kein
     * Verzeichnis, soll im aktuellen Verzeichnis verblieben werden.
     * 
     * @param toEnter
     *            das Verzeichnis, in das gewechselt werden soll
     */
    void operationEnter( File toEnter );


    /**
     * Wechselt das Verzeichnis eine Ebene nach oben. Ist das aktuelle
     * Verzeichnis bereits ein Root-Verzeichnis, wird im aktuellen Verzeichnis
     * verblieben.
     */
    void operationUp();


    /**
     * Holt die Lese-(<code>R</code>)-, Schreib-(<code>W</code>)- und Ausf�hrungs-(<code>X</code>)-Berechtigungen einer
     * Datei oder eines Verzeichnisses und gibt sie in einem String in der Form
     * "<code>RWX</code>" zur�ck. Fehlende Berechtigungen werden mit einen '<code>-</code>'
     * gekennzeichnet, z.B. wird eine Datei mit Lese- und Ausf�rungsrecht, aber
     * ohne Schreibrecht (also mit Schreib<b>schutz</b> so ausgewertet "R-X".
     * 
     * @param toGetRWX
     *            die zu untersuchende Datei bzw. das zu untersuchende
     *            Verzeichnis.
     * @return der RWX-String der Datei oder des Verzeicnisses
     * @throws NoSuchFileException 
     */
    /*
     * Throws NoSuchFileException muss hier dringend ergaenzt werden, da sonst die notwendige Fehlerbehandelung nicht funktioniert.
     */
    String operationGetRWX( File toGetRWX ) throws NoSuchFileException;


    /**
     * Erzeugt ein Verzeichnis mit dem Namen <code>name</code> im aktuellen
     * Verzeichnis
     * 
     * @param name
     *            der Name des Verzeichnisses
     * @return <code>true</code> wenn das Anlegen des Verzeichnisses erfolgreich
     *         war, sonst <code>false</code>
     */
    boolean operationCreateDir( String name );


    /**
     * Kopiert die Datei Nummer <code>source</code> in eine neue Datei mit dem
     * Namen <code>dest</code>. Diese Operation soll keine Verzeichnisse
     * kopieren. Wenn als Quelle ein Verzeichnis angegeben ist, soll
     * <code>false</code> zur�ckgegeben werden.
     * 
     * @param source
     *            Nummer der Quelldatei
     * @param dest
     *            Name der Zieldatei
     * @return <code>true</code> wenn erfolgreich kopiert wurde, sonst
     *         <code>false</code>
     */
    boolean operationCopyFile( int source, String dest );


    /**
     * Holt den aktuellen Pfad als String in der Form
     * <code>Lw:\Verzeichnis\Verzeichnis\Verzeichnis</code>, z.B.
     * <code>C:\Programme\eclipse-4.1\</code>
     * 
     * @return der String, der den Pfad enth�lt
     * @throws IOException
     *             wird weitergereicht von der File-Methode, wenn die
     *             Pfadberechnung ein Problem aufwirft.
     */
    String getCurrentPath() throws IOException;


    /**
     * Holt den Inhalt des aktuellen Verzeichnisses als String. Dabei soll jeder
     * Eintrag (s.o.) eine Zeile erhalten, die die Form
     * <code>&lt;DIR&gt; Verzeichnisname (Nummer)</code> bzw.
     * <code>Dateiname.ext (Nummer)</code> hat.
     * 
     * @return String, der den Inhalt des aktuellen Verzeichnisses beschreibt.
     */
    public String getContentAsString();


    /**
     * Rechnet eine Eintragsnummer in den entsprechenden Eintrag um.
     * 
     * @param i
     *            Nummer des Eintrags
     * @return File-Objekt, das dem Eintrag entspricht
     */
    public File numberToFile( int i );


    /**
     * Pr�ft, ob eine �bergebene Zahl eine g�ltige Eintragnummer ist, d.h.
     * <code> gr��er 0</code> und <code> kleiner Anzahl der Eintr�ge</code> ist.
     * 
     * @param i die zu untersuchende Zahl
     * @return <code>true</code> wenn die Zahl einem g�ltigen Eintrag entspricht, sonst <code>false</code>
     */
    public boolean rangeCheck( int i );

}
