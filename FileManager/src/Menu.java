/**
 * Ein halbdynamisches Konsolen-Men� auf Enum-Basis. Die Men�punkte entsprechen
 * den Enum-Konstanten. Die Klasse erzeugt selbst einen anpassbaren
 * Darstellungs-String aus den Konstanten. Sie stellt eine Methode zur
 * Verf�gung, um eingegebene Zeichen in Menueintrags-Konstanten umzuwandeln.
 * Ung�ltige Zeichen werden dabei mit einer Exception quittiert, so dass der
 * Rufer "bis zu einer g�ltigen Eingabe" eingeben lassen kann.
 * 
 * @author hmueller
 */
public enum Menu {
    ENTERDIR("[E]nter Directory"), UP("[U]p one level"), SHOWRXW(
            "[S]how privileges"), CREATEDIR("Create [D]irectory"), COPY(
            "[C]opy file"), EXIT("E[X]it program");

    /**
     * Die "Sch�ne" bzw. "Lange" Schreibweise der Men�eintr�ge
     */
    private String             pretty;

    /**
     * Die Anzahl der Men�eintr�ge pro Konsolenzeile
     */
    private static final int   itemsPerLine = 2;

    /**
     * Eine "Displayf�hige" Version des gesamten Men�s mit
     * <code>Menu.itemsPerLine</code> Eintr�gen pro Zeile.
     */
    public static final String displayable  = Menu.makeDisplayable();


    /**
     * Konstruktor
     * 
     * @param pretty
     *            die Langform dieses Eintrags
     */
    private Menu( String pretty ) {
        this.pretty = pretty;
    }


    /**
     * Liefert den "pretty String" eines Eintrags
     * 
     * @return der "Pretty String"
     */
    public String getPrettyString() {
        return this.pretty;
    }


    /**
     * Wandelt den eingegebenen Buchstaben in einen Menu-Eintrag um. Diese
     * Methode muss gepflegt werden, um die Men�eintr�ge/Konstanten in
     * <code>case</code>-Eintr�gen abzubilden.
     * 
     * @param choice
     *            der Buchstabe des Menu-Eintrags
     * @return der Menu-Eintrag, f�r den der Buchstabe stand
     * @throws IllegalArgumentException
     *             wenn ein Buchstabe �bergeben wird, der keinem Eintrag
     *             entspricht
     */
    public static Menu choiceToMenu( char choice )
            throws IllegalArgumentException {
        Menu ret = null;

        switch( Character.toUpperCase( choice ) ) {
            case 'E' : {
                ret = Menu.ENTERDIR;
                break;
            }
            case 'U' : {
                ret = Menu.UP;
                break;
            }
            case 'S' : {
                ret = Menu.SHOWRXW;
                break;
            }
            case 'D' : {
                ret = Menu.CREATEDIR;
                break;
            }
            case 'C' : {
                ret = Menu.COPY;
                break;
            }
            case 'X' : {
                ret = Menu.EXIT;
                break;
            }
            default : {
                throw new IllegalArgumentException(
                        "Illegal Argument to Menu.choiceToMenu() : " + choice );
            }
        }// eo switch

        return ret;
    }


    /**
     * Erzeugt eine "displayf�hige" Version des Men�s in einem mehrzeiligen
     * String mit <code>Menu.itemsPerLine</code> Eintr�gen pro Zeile.
     * 
     * @return der "displayf�hige" String
     */
    private static String makeDisplayable() {

        StringBuilder sb = new StringBuilder();
        int itemcount = 0;

        for( Menu m : Menu.values() ) {
            sb.append( m.getPrettyString() );
            itemcount++;
            if( itemcount % Menu.itemsPerLine == 0 ) {
                sb.append( System.getProperty( "line.separator" ) );
            } else {
                sb.append( "\t\t" );
            }
        }
        return sb.toString();

    }// eom makeDisplayable

}// eo Enum
