import java.io.IOException; //ja, nur die.
import java.nio.file.NoSuchFileException;
import java.util.*;

/**
 * View f�r die einfachen Filemanager. Diese Klasse stellt die
 * "Benutzeroberfl�che" f�r einen einfachen Filemanager zur Verf�gung. Die
 * tats�chlie Arbeit macht das engine-Objekt, dass das Interface IFileManager
 * implementieren muss.
 * 
 * @author hmueller
 */
public class FileManagerView {

    /**
     * Verbindung zur Engine, die die Arbeit macht
     */
    private IFileManager engine;

    /**
     * Verbindung zum Nutzer, der auf der tastatur herumtippt
     */
    private Scanner      sc;


    /**
     * Konstruktor - System.in finden wir selbst, aber unsere Engine muss man
     * uns mitgeben.
     * 
     * @param engine
     */
    public FileManagerView(Scanner sc,  IFileManager engine ) {
        this.engine = engine;
        this.sc = sc;
    }


    /**
     * Main()s kleiner Bruder. Diese Methode ist die "Hauptmethode", die den
     * Ablauf
     * <ol>
     * <li>Anzeige und Men�anzeige</li>
     * <li>Eingabe lesen und auf G�ltigkeit pr�fen</li>
     * <li>Operation durchf�hren</li>
     * <li>und wieder von vorn</li>
     * <ol>
     * durchf�hrt.
     */
    public void run() {

        StringBuilder sb = new StringBuilder(); // hier entsteht das Zeug, was
                                                // wir anzeigen wollen
        final String NEWLINE = System.getProperty( "line.separator" );
        String toDisplay = null; // Status- und Antwortanzeigen

        while( true ) { // ja, Endlos
            sb.delete( 0, sb.length() );// "Anzeige" leermachen
            sb.append( "ConsoleFM - the Console File Manager" + NEWLINE );
            sb.append( engine.getContentAsString() ); // Inhalt des aktuellen
                                                      // Verzeichnisses
            sb.append( NEWLINE );
            try {
                sb.append( engine.getCurrentPath() ); // aktuelles Verzeichnis
            }
            catch( IOException ex ) {
                sb.append( ex.getMessage() );
            }
            sb.append( NEWLINE );
            sb.append( Menu.displayable );// Men�
            if( toDisplay != null ) {
                sb.append( NEWLINE + NEWLINE + toDisplay );// Statusmeldung,
                                                           // wenn es eine gibt
                toDisplay = null;
            }

            this.display( sb.toString() );// Anzeigen

            Menu op = this.choose(); // Ausw�hlen lassen

            // Auswertung und Ansto�en der Operation
            switch( op ) {
                case ENTERDIR : {
                    System.out.println( "Enter the number of a directory: " );
                    this.engine.operationEnter( this.engine.numberToFile( this
                            .enterNumber() ) );
                    break;
                }

                case CREATEDIR : {
                    System.out.println( "Enter a name : " );
                    this.engine.operationCreateDir( this.sc.next() );
                    break;
                }

                case SHOWRXW : {
                    System.out
                            .println( "Enter the number of a file or directory: " );
                    int number = this.enterNumber();
                    try {
			toDisplay =
			        this.engine.operationGetRWX( this.engine
			                .numberToFile( number ) )
			                + " "
			                + this.engine.numberToFile( number )
			                        .getName();
		    } catch (NoSuchFileException e) {
			// 
		    }

                    break;
                }
                case UP : {
                    this.engine.operationUp();
                    break;
                }
                case EXIT : {
                    System.out.println( "Bye!" );
                    System.exit( 0 );
                    break;
                }
                default : {
                    break;
                }

            }// eo case

        }

    }// eom run()


    /**
     * Macht die Konsolenanzeige leer und gibt den Anzeige-String aus
     * 
     * @param toDisplay
     *            der String, der den zuk�nfiten Bildschirminhalt enth�lt
     */
    public void display( String toDisplay ) {
        System.out
                .printf( "%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n" );
        System.out.println( toDisplay );
    }


    /**
     * Liest eine Zahl von der Tastatur ein - so lange, bis
     * <ul>
     * <li>wirklich eine Zahl eingeben wurde,</li>
     * <li>diese Zahl eine g�ltige Eintragsnummer ist,</li>
     * </ul>
     * ... und gibt diese Zahl zur�ck
     * 
     * @return die gepr�fte Zahl
     */
    private int enterNumber() {
        int ret = -1;

        while( ret == -1 ) {
            try {
                ret = -1 + sc.nextInt();
                if( !this.engine.rangeCheck( ret ) ) {
                    throw new InputMismatchException();
                }
            }
            catch( InputMismatchException ex ) {
                sc.nextLine();
                System.out
                        .println( "The Number must correspond with an entry!" );
                ret = -1;
            }
        }
        return ret;
    }


    /**
     * Liest eine Men�eingabe ein - so lange, bis eine g�ltige Eingabe get�tigt
     * wurde. Die "Umrechung" in eine <code>Menu</code>-Konstante erfolgt nicht
     * hier, sondern in der entsprechenden Methode des Enums.
     * 
     * @return <code>Menu</code>-Konstante, die dem Men�eintrag entspricht
     */
    private Menu choose() {
        Menu ret = null;
        char read = ' ';

        while( read == ' ' ) {
            try {
                read = sc.next( "." ).charAt( 0 );
                ret = Menu.choiceToMenu( read );
            }
            catch( NoSuchElementException ex ) {
                read = ' ';
                sc.nextLine();
            }
            catch( IllegalArgumentException ex ) {
                read = ' ';
                System.out
                        .println( "Choice not available, use a [B]racked [L]etter." );
                sc.nextLine();
            }
        }// eo while

        return ret;
    }
}// eoc
