import java.io.*;
import java.util.*;

/**
 * Starter-Klasse f�r den File-Manager. Stellt mit main() den Einsprungpunkt f�r
 * die VM, stellt sicher, dass ein lauff�higer Ausgangszustand vorliegt. Falls
 * erforderlich wird dieser Zustand durch den Starter hergestellt. Erstellt ein
 * View- und ein Engine-Objekt und l�sst das View-Objekt anlaufen.
 * 
 * @author hmueller
 */
public class FileManagerStarter {

    /**
     * @param args
     *            hier sollte von der Kommandozeile ein Start-Verzeichnis
     *            mitgegeben werden
     */
    public static void main( String[] args ) {

        File startDir = null;
        Scanner sc = new Scanner( System.in );
        SortedSet<File> roots =
                new TreeSet<File>( Arrays.asList( File.listRoots() ) );

        // Wenn kein Startverzeichnis mitgegeben wurde...
        if( args.length > 0 ) {
            startDir = new File( args[0] );
        } else {

            // holen wir die Laufwerke dieses Computers und lassen den Benutzer
            // eins ausw�hlen. In der Root dieses Laufwerks fangen wir dann an.
            System.out.println( "No Drive specified." );
            for( File f : roots ) {
                System.out.println( f );
            }
            
            String driveLetter = null;
            File f = null;

            //So lange w�hlen lassen, bis etwas g�ltiges herauskommt...
            while( driveLetter == null ) {
                try {
                    System.out.println( "Choose a drive letter: " );
                    driveLetter = sc.next( "[A-Za-z]{1}" );
                    f = new File( driveLetter + ":\\" );
                    if( !roots.contains( f ) ) {
                        throw new InputMismatchException();
                    }
                }
                catch( InputMismatchException ex ) {
                    driveLetter = null;
                }
            }
            startDir = f;
        }

        //View-Objekt und Engine-Objet erzeugen
        FileManagerView view =
                new FileManagerView( sc,  new FileManagerEngine( startDir ) );
        view.run(); //los geht's
       

    }

}
