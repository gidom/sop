/**
 * 
 */
package log2;

import static log2.BooleanOperator.OR;

import javax.xml.bind.JAXB;

/**
 * @author sigma
 *
 */
public class Test {
    public static void main(String[] args) {
	Input a = new Input("A", false);
	Input b = new Input("B", false);
	Input c = new Input("C", false);
	
	Gate g1 = new Gate(OR);
	g1.addExpression(b);
	g1.addExpression(c);
//	System.out.println(g1.getValue());
	
	Gate g = new Gate(OR);
	g.setName("OROR");
	g.addExpression(g1);
	g.addExpression(a);
	
//	File f = new File("schaltung.xml");
	
//	System.out.println(g.getValue());
	
//	JAXB.marshal(g, f);
	JAXB.marshal(g, System.out);
	
//	Gate g2 = JAXB.unmarshal(f, Gate.class);
	
//	System.out.println(g2.getValue());
    }
}
