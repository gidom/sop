/**
 * 
 */
package log2;

import javax.xml.bind.annotation.XmlEnum;

/**
 * @author sigma
 *
 */
@XmlEnum(String.class)
public enum ExpressionType {
    CIRCUIT,
    FORMULA;
}
