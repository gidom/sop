/**
 * 
 */
package log2;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author sigma
 * 
 */
@XmlRootElement(name = "expression")
public class Gate extends Expression {

    protected String name;

    @XmlElement(name = "operator", type = BooleanOperator.class)
    protected BooleanOperator boolOperator;

    @XmlElements({ @XmlElement(name = "input", type = Input.class),
	    @XmlElement(name = "gate", type = Gate.class) })
    protected List<Expression> exp = new ArrayList<Expression>();

    /**
     * @param logic
     */
    public Gate(BooleanOperator bo) {
	super();
	this.boolOperator = bo;
    }

    /**
     * 
     */
    public Gate() {
	super();
    }

    /**
     * @param exp
     *            the exp to set
     */
    public void setExp(List<Expression> exp) {
	this.exp = exp;
    }

    public void addExpression(Expression e) {
	exp.add(e);
    }

    /**
     * @return the name
     */
    @XmlAttribute
    public String getName() {
	return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
	this.name = name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see log2.Expression#isInput()
     */
    @Override
    public boolean isInput() {
	// TODO Auto-generated method stub
	return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see log2.Expression#getValue()
     */
    @Override
    public boolean getValue() {
	boolean result = true;
	for (Expression e : exp) {
	    result = boolOperator.link(result, e.getValue());
	}
	return result;
    }

    // public String toString() {
    // StringBuilder sb = new StringBuilder();
    // return "Y = " +
    //
    // }

}
