/**
 * 
 */
package log2;

/**
 * @author sigma
 *
 */
public class OrGate extends Gate {

    /* (non-Javadoc)
     * @see log2.Expression#getValue()
     */
    @Override
    public boolean getValue() {
	boolean result = true;
	for (Expression e : exp) {
	    result |= e.getValue();
	}
	return result;
    }

}
