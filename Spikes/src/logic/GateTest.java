package logic;

import static logic.Logic.*;

import javax.xml.bind.JAXB;

/**
 * @author sigma
 * 
 */
public class GateTest {
    public static void main(String[] args) {


	 
	 InputExpression i1 = new InputExpression("A");
	 InputExpression i2 = new InputExpression("B");
	 InputExpression i3 = new InputExpression("C");
	 
	 
	 Gate gate = new Gate(AND);
	 
	 gate.addExpression(i1);
	 gate.addExpression(i2);
	 gate.addExpression(i3);
	 
	 i1.setTrue();
	 i2.setFalse();
	 i3.setTrue();
	
	 JAXB.marshal(i1, System.out);
	 
	 
//	 JAXB.marshal(gate, System.out);

	 
	boolean result = gate.getValue();

	System.out.println(result);
    }
}
