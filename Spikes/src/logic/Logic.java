package logic;
/**
 * 
 */

/**
 * @author sigma
 * 
 */
public enum Logic {
    AND {
	public boolean link(boolean... a) {
	    boolean result = true;
	    for (boolean x : a)
		result &= x;
	    return result;
	}
    },
    OR {
	public boolean link(boolean... a) {
	    boolean result = true;
	    for (boolean x : a)
		result |= x;
	    return result;
	}
    },
    NOT;

    public boolean link(boolean... a) {
	return !a[0];
    }

}
