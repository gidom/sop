package logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 */

/**
 * @author dominik@gierczak.net
 * 
 */
@XmlRootElement
public class Gate implements Expression {

    private List<Expression> exp = new ArrayList<Expression>();

    private Map<String, Boolean> inputMap = new HashMap<String, Boolean>();

    @XmlAttribute
    private Logic logic;

    public Gate(Logic logic) {
	this.logic = logic;
    }
    
    public Gate() {
	
    }

    /**
     * Returns the list of input names.
     * 
     * @return list of names
     */
    public Set<String> getInputList() {
	return inputMap.keySet();
    }

    void setInputMap(Map<String, Boolean> inputMap) {
	this.inputMap = inputMap;
    }

    public void setInput(String key, boolean value) {
	inputMap.put(key, value);
    }

    /**
     * Adds new Expressions to this Gate.
     * 
     * @param e
     *            Expression
     */
    public void addExpression(Expression e) {
	if (e.isInput()) {
	    inputMap.put(((InputExpression) e).getName(), false);
	} else {
	    ((Gate) e).setInputMap(inputMap);
	}
	exp.add(e);
	
    }

    @XmlElement (name="expression")
    public List<Expression> getInputs() {
	return exp;
    }

    /*
     * (non-Javadoc)
     * 
     * @see logic.Expression#getValue()
     */
    @Override
    public boolean getValue() {
	boolean[] values = new boolean[exp.size()];
	for (int i = 0; i < exp.size(); i++) {
	    values[i] = exp.get(i).getValue();
	}
	return logic.link(values);
    }

    /*
     * (non-Javadoc)
     * 
     * @see logic.Expression#isInput()
     */
    @Override
    public boolean isInput() {
	return false;
    }
}
