/**
 * 
 */
package junittest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author sigma
 * 
 */
public class StringUtilsTest {

	/**
	 * Test method for {@link junittest.StringUtils#reverse(java.lang.String)}.
	 */
	@Test
	public final void testReverse() {
		assertEquals("", StringUtils.reverse(""));
		assertEquals("cba", StringUtils.reverse("abc"));
	}

}
