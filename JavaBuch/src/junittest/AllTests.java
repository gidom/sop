/**
 * 
 */
package junittest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author sigma
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ StringUtilsTest.class })
public class AllTests {

}
