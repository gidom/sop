/**
 * 
 */
package MoreFunWithWildcards;

import java.util.Collection;
import java.util.List;

/**
 * @author sigma
 * @param <T>
 *
 */
public class Wildcards {
	public static <T> T writeAll(Collection<T> coll, Sink<T> snk) {
		T last = null;
		for (T t: coll) {
			last = t;
			snk.flush(t);
		}
		return last;
	}
	
	void printCollection(Collection<?> c) {
		for (Object e : c) {
			System.out.println(e);
			
		}
		
	}
	
	public static void toTo(List<? super Wildcards> t) {
		System.out.println(t.toString());
	}
	
	@Override
	public String toString() {
		return this.getClass().getCanonicalName();
	}
	
	public static void main(String[] args) {
	}
}
