/**
 * 
 */
package MoreFunWithWildcards;

/**
 * @author sigma
 *
 */
public interface Sink <T> {
	void flush(T t);
}
