/**
 * 
 */
package testpackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * @author sigma
 * 
 */
public class Main {

	static int i = 0;

	static int test() {
		try {
			return i;
		} finally {
			i++;
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(test());
		System.out.println(i);
	}

}
