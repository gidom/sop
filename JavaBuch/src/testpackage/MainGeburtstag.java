/**
 * 
 */
package testpackage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;

import javabuch5.Geburtstag;
import javabuch5.Geburtstagskalender;
import javabuch5.Person;

/**
 * @author sigma
 * 
 */
public class MainGeburtstag {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String path = "d:/Users/sigma/Desktop";
		String file = "geburtstage.txt";
		Geburtstagskalender cal = new Geburtstagskalender();
		Collection<String[]> sColl = new ArrayList<String[]>();

		readFile(sColl, new File(path, file));
		
		for (String[] s: sColl) {
			Calendar c = new  GregorianCalendar(Integer.valueOf(s[1]),Integer.valueOf(s[2]),Integer.valueOf(s[3]));
			Person p = new Person(s[0], new Geburtstag(c));
			cal.fuegeHinzu(p);
		}
		Collections.sort((List<Person>)cal.personen());
		System.out.println(cal.toString());
		System.out.println("jub: " + cal.naechsterJubilar());
		System.out.println();
		
		Collections.sort((List<Person>) cal.personen(), new Geburtstagskalender().new GeburtstagsComparator<Person>());
		System.out.println(cal.toString());
		
	}

	/**
	 * @param sColl
	 */
	public static void readFile(Collection<String[]> sColl, File file) {
		try (Scanner sc = new Scanner(new BufferedReader(new FileReader(file)))) {

			while (sc.hasNextLine()) {
				String s = sc.nextLine();
				if (!s.startsWith("#")) {
					String[] sArray = s.split(",");
					sColl.add(sArray);
				}
			}
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}
}