/**
 * Funktion zur Berrechnung der Quersumme einer Zahl 
 */

import java.io.*;

public class Quersumme {

	static String readLine(String s) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print(s);
		return br.readLine();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		int quersumme = 0;
		String zahl = readLine("Zahl: ");
		
		for (int i = 0; i < zahl.length(); i++) {
			quersumme += Integer.parseInt(zahl.substring(i, i+1));
			
		}
		System.out.println(quersumme);
				
	}
}
