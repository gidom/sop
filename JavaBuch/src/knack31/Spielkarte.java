/**
 * 
 */
package knack31;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author sigma
 * 
 */
public class Spielkarte implements Comparable<Spielkarte> {
	private Farbe farbe;
	private Zahl zahl;

	/**
	 * @param farbe
	 * @param zahl
	 */
	public Spielkarte(Farbe farbe, Zahl zahl) {
		super();
		this.farbe = farbe;
		this.zahl = zahl;
	}

	public static Collection<Spielkarte> getKartenspiel() {
		Collection<Spielkarte> c = new ArrayList<Spielkarte>();
		for (Farbe f: Farbe.values())
			for(Zahl z: Zahl.values())
				c.add(new Spielkarte(f,z));
		return c;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return farbe + " " + zahl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Spielkarte o) {
		int w1 = this.farbe.ordinal() * 8 + this.zahl.ordinal();
		int w2 = o.farbe.ordinal() * 8 + o.zahl.ordinal();
		return w1 - w2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((farbe == null) ? 0 : farbe.hashCode());
		result = prime * result + ((zahl == null) ? 0 : zahl.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Spielkarte)) {
			return false;
		}
		Spielkarte other = (Spielkarte) obj;
		if (farbe != other.farbe) {
			return false;
		}
		if (zahl != other.zahl) {
			return false;
		}
		return true;
	}

}
