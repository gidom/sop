/**
 * 
 */
package javabuch5.tests;


import static net.gierczak.buffbit.util.Calendar.DATE;
import static net.gierczak.buffbit.util.Calendar.MONTH;
import static net.gierczak.buffbit.util.Calendar.YEAR;
import static org.junit.Assert.assertEquals;
import javabuch5.Geburtstag;
import net.gierczak.buffbit.util.Calendar;

import org.junit.Test;

/**
 * @author sigma
 * 
 */
public class GeburtstagTest {
	// useful constants to modify if needed
	private static final int DISTANCE_YEARS = 10;
	private static final int DISTANCE_MONTHS = 10;
	private static final int DISTANCE_DAYS = 10;
	private final boolean INCREASE = true;
	private final boolean DECREASE = false;

	private Calendar current = new Calendar();;

	/**
	 * Test method for {@link javabuch5.Geburtstag#alter()}.
	 */
	@Test
	public final void testAlter() {

		// today - DISTANCE_YEARS, minus DISTANCE_MONTHS
		assertEquals(
				DISTANCE_MONTHS,
				new Geburtstag(new Calendar().turn(YEAR, -DISTANCE_YEARS).turn(
						MONTH, -DISTANCE_MONTHS)).alter());

		// today - DISTANCE_YEARS, minus DISTANCE_DAYS
		assertEquals(
				DISTANCE_YEARS,
				new Geburtstag((new Calendar().turn(YEAR, -DISTANCE_YEARS))
						.turnDays(DISTANCE_DAYS, false)).alter());

		// today - DISTANCE_YEARS, minus one day
		assertEquals(DISTANCE_YEARS,
				new Geburtstag(new Calendar().turn(YEAR, -DISTANCE_YEARS)
						.turnDays(1, false)).alter());

		// today - DISTANCE_YEARS
		assertEquals(DISTANCE_YEARS,
				new Geburtstag(new Calendar().turn(YEAR, -DISTANCE_YEARS))
						.alter());

		// today - DISTANCE_YEARS, plus one day
		assertEquals(DISTANCE_YEARS - 1,
				new Geburtstag(new Calendar().turn(YEAR, -DISTANCE_YEARS)
						.turnDays(1, true)).alter());

		// today - DISTANCE_YEARS, plus DISTANCE_DAYS
		assertEquals(DISTANCE_YEARS - 1,
				new Geburtstag(new Calendar().turn(YEAR, -DISTANCE_YEARS)
						.turnDays(DISTANCE_DAYS, true)).alter());

		// today - DISTANCE_YEARS, plus DISTANCE_MONTHS
		assertEquals(
				DISTANCE_MONTHS,
				new Geburtstag(new Calendar().turn(YEAR, -DISTANCE_YEARS).turn(
						MONTH, -DISTANCE_MONTHS)).alter());

	}

	/**
	 * Test method for {@link javabuch5.Geburtstag#naechsteGeburtstagsfeier()}.
	 */

	@Test
	public final void testNaechsteGeburtstagsfeier() {

		Calendar assertDate = new Calendar(current.get(YEAR) + 1,
				current.get(MONTH), current.get(DATE) - 1);
		Calendar testDate = new Calendar(current.get(YEAR) - 10,
				current.get(MONTH), current.get(DATE) - 1);

		assertEquals(assertDate,
				new Geburtstag(testDate).naechsteGeburtstagsfeier());

		assertEquals(assertDate.turn(YEAR, DECREASE).turn(DATE, INCREASE),
				new Geburtstag(testDate.turn(DATE, INCREASE))
						.naechsteGeburtstagsfeier());

		assertEquals(assertDate.turn(DATE, INCREASE),
				new Geburtstag(testDate.turn(DATE, INCREASE))
						.naechsteGeburtstagsfeier());

		assertEquals(assertDate.turn(MONTH, INCREASE),
				new Geburtstag(testDate.turn(MONTH, INCREASE))
						.naechsteGeburtstagsfeier());

	}

	/**
	 * Test method for
	 * {@link javabuch5.Geburtstag#compareTo(javabuch5.Geburtstag)}.
	 */
	@Test
	public final void testCompareTo() {
		assertEquals(-1,
				new Geburtstag(new Calendar(2002, 9, 3))
						.compareTo(new Geburtstag(new Calendar(2002, 9, 4))));
		assertEquals(0,
				new Geburtstag(new Calendar(2002, 9, 3))
						.compareTo(new Geburtstag(new Calendar(2002, 9, 3))));
		assertEquals(1,
				new Geburtstag(new Calendar(2002, 9, 5))
						.compareTo(new Geburtstag(new Calendar(2002, 9, 4))));
	}
}
