/**
 * 
 */
package javabuch5;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author sigma
 * 
 */
public class Geburtstagskalender {
	List<Person> c = new ArrayList<Person>();

	public class GeburtstagsComparator<T extends Person> implements
			Comparator<T> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(Person o1, Person o2) {
			return o1.getGeb().compareTo(o1.getGeb());
		}

	}
	
	

	/**
	 * F�gt die Person zum Geburtstagskalender hinzu und liefert true zur�ck.
	 * Wenn die Person bereits im Geburtstagskalender enthalten ist, wird der
	 * Geburtstagskalender nicht ver�ndert und false zur�ckgegeben.
	 * 
	 * @param p
	 *            Person die hinzugefuegt wird
	 * @return true falls der Geburtstagskalender geaendert wird, sonst false
	 */
	public boolean fuegeHinzu(Person p) {
		return c.add(p);
	}

	/**
	 * Entfernt die Person aus dem Geburtstagskalender und liefert true zur�ck;
	 * falls die Person nicht im Geburtstagskalender enthalten ist und somit
	 * nicht daraus entfernt werden kann, wird false zur�ckgegeben.
	 * 
	 * @param p
	 *            Person die entfernt werden soll
	 * @return true falls der Geburtstagskalender geaendert wird, sonst false
	 */
	public boolean entferne(Person p) {
		return c.remove(p);
	}

	/**
	 * Liefert die Datenstruktur zur�ck, in der die Personen sortiert
	 * gespeichert sind.
	 * 
	 * @return Datenstruktur mit den enthaltenen Personen
	 */
	public Collection<Person> personen() {
		return c;
	}

	/**
	 * Liefert die Person zur�ck, die (von heute an gerechnet) als n�chstes
	 * Geburtstag hat.
	 * 
	 * @return den naechsten Jubilar
	 */
	public Person naechsterJubilar() {
		Collections.sort(c, new GeburtstagsComparator<Person>());
		return c.get(0);

	}

	/**
	 * Liefert die Personen zur�ck, die am angegebenen Datum in einem beliebigen
	 * Jahr geboren wurden (monat zwischen 1 und 12).
	 * 
	 * @param monat
	 * @param tag
	 * @return
	 */
	@Deprecated
	public Collection<Person> jubilare(int monat, int tag) {
		Collection<Person> cp = new ArrayList<Person>();
		for (Person p : c) {
			int pmonat = p.getGeb().naechsteGeburtstagsfeier()
					.get(Calendar.MONTH);
			int ptag = p.getGeb().naechsteGeburtstagsfeier()
					.get(Calendar.DAY_OF_MONTH);
			if (pmonat == monat && ptag == tag)
				cp.add(p);
		}
		return cp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Person p : c)
			sb.append(p.toString() + "\n");
		return sb.toString();
	}
}
