/**
 * Eine Person soll �ber Vor- und Nachnamen sowie �ber einen Geburtstag verf�gen. 
 */
package javabuch5;

import java.util.Calendar;

/**
 * @author sigma
 * 
 */
public class Person implements Comparable<Person> {

	private String name;
	private Geburtstag geb;

	/**
	 * @param name
	 * @param vorname
	 * @param geb
	 */
	public Person(String name, Geburtstag geb) {
		super();
		this.name = name;
		this.geb = geb;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the geb
	 */
	public Geburtstag getGeb() {
		return geb;
	}

	/**
	 * @param geb
	 *            the geb to set
	 */
	public void setGeb(Geburtstag geb) {
		this.geb = geb;
	}

	/**
	 * Personen werden alphabetisch nach Nachnamen, dann alphabetisch nach
	 * Vornamen und schlie�lich aufsteigend nach ihrem Alter sortiert. Sind
	 * Nachname, Vorname und Geburtstag zweier Personen gleich, so werden die
	 * Personen als �gleich � angesehen.
	 */
	@Override
	public int compareTo(Person p) {
		// use of case insensitive comparator for strings
		return String.CASE_INSENSITIVE_ORDER.compare(getName() + geb.alter(),
				p.getName() + p.geb.alter());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name + " "
				+ geb.naechsteGeburtstagsfeier().get(Calendar.DAY_OF_MONTH)
				+ "." + geb.naechsteGeburtstagsfeier().get(Calendar.MONTH);

	}

}
