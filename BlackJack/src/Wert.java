/**
 * Der Wert wird als Enum abgebildet.
 */
public enum Wert {
	ZWEI(2), DREI(3), VIER(4), FUENF(5), SECHS(6), SIEBEN(7), ACHT(8), NEUN(9), ZEHN(
			10), BUBE(10), DAME(10), KOENIG(10), ASS(11);

	private int punkte;

	private Wert(int punkte) {
		this.punkte = punkte;
	}

	/**
	 * Gibt die Punkte des Wertes zurueck.
	 * @return Punkte des Wertes
	 */
	public int getPunkte() {
		return punkte;
	}
	
	public String toString() {
		return name().charAt(0) + name().toLowerCase().substring(1, name().length());
	}
	
	
}
