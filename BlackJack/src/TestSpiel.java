public class TestSpiel {

	public static void main(String[] args) {
		Spiel testspiel = new Spiel();
		
		Spieler spieler1 = new Spieler("Hans");
		spieler1.setEntscheider(EntscheiderAuswahl.ENTSCHEIDERMENSCH.getEntscheider());
		Spieler spieler2 = new Spieler ("Bank");
		spieler2.setEntscheider(EntscheiderAuswahl.ENTSCHEIDERCIMMERFALSE.getEntscheider());
				
		testspiel.addSpieler(spieler1);
		testspiel.addSpieler(spieler2);
		testspiel.los();
		
	}
}
