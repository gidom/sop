public class EntscheiderSummeSchwellwert implements Entscheider {

	/**
	 * Maximalwert f�r diesen Spieler.
	 */
	public final int SCHWELLWERT = 15;

	public boolean sollNeueKarte(Hand dieHand) {
		if (dieHand.getPunkte() <= SCHWELLWERT) {
			return true;
		} else {
			return false;
		}
	}

}
