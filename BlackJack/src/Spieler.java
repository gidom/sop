public class Spieler {

	private Hand hand;
	private Entscheider entscheider;
	/**
	 * MAX auf 21 festgelegt;
	 */
	protected final int MAX = 21;
	/**
	 * Name des Spieler
	 */
	private String name;

	public Spieler(String derName) {
		this.name = derName;
		hand = new Hand();
	}

	public String getName() {
		return name;
	}

	public Hand getHand() {
		return hand;
	}

	public void setEntscheider(Entscheider e) {
		this.entscheider = e;
	}

	public boolean isNotVerkauft() {
		return hand.getPunkte() <= MAX;
	}

	public boolean aufnehmen(Karte k) {
		boolean handAufgenommen = hand.aufnehmen(k);
		System.out.println("Spieler " + getName() + " hat " + k.toString()
				+ " aufgenommen.\nSein aktueller Punktestand lautet: "
				+ getHand().getPunkte() + "\nSeine Hand: "
				+ getHand().toString() + ".\n");
		return handAufgenommen;
	}

	public Entscheider getEntscheider() {
		return entscheider;
	}

	public boolean sollNeueKarte() {
		return entscheider.sollNeueKarte(hand);
	}
}
