/**
 * 
 */

public class KonsolenAusgabe implements Ausgabe {
	/*
	 * (non-Javadoc)
	 * 
	 * @see Ausgabe#printEntscheidung(Spieler, boolean)
	 */
	public void printEntscheidung(Spieler spieler, boolean karteZiehen) {
		if (!spieler.getEntscheider().getClass().getName()
				.equalsIgnoreCase("EntscheiderMensch")) {
			System.out.println(karteZiehen ? "Jo!" : "Noe!");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Ausgabe#printUebersicht(Spieler)
	 */
	public void printUebersicht(Spieler spieler) {
		System.out.println("Spieler " + spieler.getName() + " Sie haben "
				+ spieler.getHand().getPunkte()
				+ " Punkte. Wollen Sie eine weitere Karte aufnehmen?");
	}
}
