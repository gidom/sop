/**
 * Die Farbe wird als Enum abgebildet.
 */

public enum Farbe {
	KARO, HERZ, PIK, KREUZ;
	
	public String toString() {
		return name().charAt(0) + name().toLowerCase().substring(1, name().length());
	}
}
