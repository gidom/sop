public interface Ausgabe {

	/**
	 * Gibt die Entscheidung nur dann visuell aus, wenn der Entscheider kein Mensch ist.
	 * @param spieler Spieler
	 * @param karteZiehen Gibt an, ob eine Karte gezogen wird.
	 */
	public void printEntscheidung(Spieler spieler, boolean karteZiehen);

	/**
	 * Gibt eine Uebersicht des aktuellen Spielzustandes aus
	 * @param spieler
	 */
	public void printUebersicht(Spieler spieler);

}