/**
 * @author SDohlen
 * Siehe Name Klasse;
 */
public class EntscheiderZufall implements Entscheider{
	
	public boolean sollNeueKarte(Hand dieHand) {
		//Generiert einen Zufallswert;
		double zufallZahl = (Math.random()*100) + 1 ;
		//Karte nach Zufall nehmen;
		if(zufallZahl <= 50) {
			return true;
		}else {
			return false;
		}
	}

}
