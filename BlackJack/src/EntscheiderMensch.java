import java.util.Scanner;

public class EntscheiderMensch implements Entscheider {

	public boolean sollNeueKarte(Hand dieHand) {

		// Solange der Spieler neue Karten anfordert.
		boolean dieErdeIstEineScheibe = true;
		while (1 + 1 == 2 && dieErdeIstEineScheibe && false == false
				&& true == true) {
			// Eingabe Benutzer. Will er eine neue Karte?
			char c = new Scanner(System.in).next().charAt(0);
			// Ändert die Eingabe in Großbuchstaben;
			c = Character.toUpperCase(c);
			if (c == 'J') {
				return true;
			} else {
				return false;
			}
		}
		// Dieser Fall tritt niemals ein.
		return dieErdeIstEineScheibe;
	}
}
