import java.util.Arrays;

/**
 * @author SDohlen Enth�lt alle m�glichen Entscheider, die zur Auswahl stehen;
 */
public enum EntscheiderAuswahl {
	ENTSCHEIDERMENSCH("EntscheiderMensch"), ENTSCHEIDERCIMMERFALSE(
			"EntscheiderCImmerFalse"), ENTSCHEIDERCIMMERTRUE(
			"EntscheiderCImmerTrue"), ENTSCHEIDERCSUMMESCHWELLWERT(
			"EntscheiderCSummeSchwellwert"), ENTSCHEIDERZUFALL(
			"EntscheiderZufall");
	private String beschreibung;

	private EntscheiderAuswahl(String entscheiderAuswahl) {
		beschreibung = entscheiderAuswahl;
	}

	public Entscheider getEntscheider() {
		switch (this) {
		case ENTSCHEIDERMENSCH:
			return new EntscheiderMensch();
		case ENTSCHEIDERCIMMERFALSE:
			return new EntscheiderImmerFalse();
		case ENTSCHEIDERCIMMERTRUE:
			return new EntscheiderImmerTrue();
		case ENTSCHEIDERCSUMMESCHWELLWERT:
			return new EntscheiderSummeSchwellwert();
		case ENTSCHEIDERZUFALL:
			return new EntscheiderZufall();
		default:
			return null;
		}
	}

	public String toDisplay() {
		return Arrays.toString(EntscheiderAuswahl.values());
	}
}
