import java.util.*;

/**
 * @author dgierczak
 * 
 */
public class Hand {
	private List<Karte> karten;
	

	public Hand() {
		karten = new ArrayList<Karte>();
	}
	
	public int getPunkte() {
		int summe = 0;
		for (Karte k : karten)
			summe += k.getWert().getPunkte();
		return summe;

	}

	public boolean aufnehmen(Karte karte) {
		return karten.add(karte);
	}

	public int getAnzahl() {
		return karten.size();
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Karte k : karten) {
			sb.append(k.toString() + ", ");
		}
		return sb.toString();
	}

}
