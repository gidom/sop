import java.util.Arrays;
import java.util.Collections;
import java.util.Stack;

public class Stapel {
	private Stack<Karte> stapel = new Stack<Karte>();

	/**
	 * @param stapel
	 */
	public Stapel() {
		for (Wert w : Wert.values()) {
			for (Farbe f : Farbe.values()) {
				stapel.push(new Karte(f, w));
			}
		}
		mischen();
	}

	public void mischen() {
		Collections.shuffle(stapel);
	}

	/**
	 * Gibt die oberste Karte des Stapels zurueck. Ist der Stapel leer, wird
	 * <code>null</code> zurueckgegeben.
	 * 
	 * @return die oberste Karte oder null falls Stapel leer
	 */
	public Karte gibKarte() {
		if (!stapel.empty())
			return stapel.pop();
		else
			return null;
	}

	public String toString() {
		Karte[] k = new Karte[0]; 
		k = stapel.toArray(k);
		return Arrays.toString(k);
	}
}
