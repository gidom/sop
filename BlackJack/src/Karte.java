/**
 * @author dgierczak
 * 
 */
public class Karte {
	private Farbe farbe;
	private Wert wert;

	/**
	 * @param farbe
	 * @param wert
	 */
	public Karte(Farbe farbe, Wert wert) {
		super();
		this.farbe = farbe;
		this.wert = wert;
	}

	/**
	 * @return the farbe
	 */
	public Farbe getFarbe() {
		return farbe;
	}

	/**
	 * @return the wert
	 */
	public Wert getWert() {
		return wert;
	}

	@Override
	public String toString() {
		return "" + farbe.toString() + " " + wert.toString();
	}
}
