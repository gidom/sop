import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class Spiel {

	/**
	 * Wieviele Spieler sind im Spiel?
	 */
	private List<Spieler> spielerListe;

	/**
	 * Eine Karte muss ja schlie�lich gezogen werden;
	 */
	private Stapel karten;
	
	/** 
	 * Definition der visuellen Standardausgabe des Spiels
	 */
	private Ausgabe ui = new KonsolenAusgabe();

	/**
	 * Konstruktor erstellt direkt die Liste bei neuem Spiel;
	 */
	public Spiel() {
		spielerListe = new ArrayList<Spieler>();
		karten = new Stapel();
	}

	/**
	 * @param sp
	 *            F�gt Spieler hinzu
	 */
	public void addSpieler(Spieler sp) {
		spielerListe.add(sp);
	}

	/**
	 * @return Das Spiel gilt erst dann als bereit, wenn auch mindestens zwei
	 *         Spieler teilnehmen;
	 */
	public boolean istBereit() {
		if (spielerListe.size() >= 2) {
			return true;
		} else {
			return false;
		}
	}

	public void los() {
		/*
		 * An dieser Stelle erh�lt jeder Spieler erstmal zwei Karten;
		 */
		for (int i = 0; i < 2; i++) {
			for (Spieler sp : spielerListe) {
				sp.aufnehmen(karten.gibKarte());
			}
		}
		
		boolean spielNichtBeendet = true;

		while (spielNichtBeendet) {

			// Variable enth�lt L�nge der liste;
			int verbliebeneSpieler = spielerListe.size();

			/*
			 * Iterator "it" dient mir als Zeiger auf die eigentliche Liste
			 * "spielerListe". M�glichkeit ohne FOREACH die Liste zu durchlaufen
			 */
			Iterator<Spieler> it = spielerListe.iterator();

			/*
			 * Solange ein Element an dieser Stelle in der Liste vorhanden und
			 * die Variable "spielNichtBeendet" = true ist wird diese Schleife
			 * durchlaufen;
			 */
			while (it.hasNext() && spielNichtBeendet) {

				// Gibt das aktuelle Element zur�ck und Springt zum n�chsten
				// Element (Schleifendurchlauf-> (i++))
				Spieler sp = it.next();

				// Wenn Spieler 21 hat, dann Spiel beendet;
				if (sp.getHand().getPunkte() == 21) {
					spielNichtBeendet = false;
				} else { // Sonst nimm Karte auf;
					boolean karteZiehen = sp.sollNeueKarte();
					// Ausgabe des Spiels in der Benutzeroberflaeche
					ui.printUebersicht(sp);
					ui.printEntscheidung(sp, karteZiehen);

					if (karteZiehen) {
						sp.aufnehmen(karten.gibKarte());
						// Wenn spieler 21 �berschritten, dann Spiel beendet;
						if (!sp.isNotVerkauft()) {
							spielNichtBeendet = false;
						}
					}
					/*
					 * Nehmen Spieler keine Karte auf, wird der Z�hler (kompl
					 * Spieleranzahl) reduziert;
					 */
					else {
						verbliebeneSpieler--;
					}
				}
			}
			// Wenn kein Spieler eine Karte aufnimmt, ist das Spiel
			// ebenfalls beendet;
			if (verbliebeneSpieler == 0) {
				spielNichtBeendet = false;
			}
		}
	}
}
