package satzgenerator;

import java.util.Random;

import javax.swing.JFrame;

public class Satzgenerator extends JFrame{
	String[][] s = { { "the", "a", "one", "some", "any" },
			{ "boy", "dog", "car", "bicycle" },
			{ "ran", "jumped", "sang", "moves" },
			{ "away", "towards", "around", "near" } };

	public String generate() {
		StringBuilder sb = new StringBuilder();
		Random r = new Random();
		for (int i = 0; i < 6; i++) {
			sb.append((s[i % 4][r.nextInt(s[i % 4].length)]))
					.append(i < 5 ? " " : ".");
		}
		sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
		return sb.toString();
	}
}
