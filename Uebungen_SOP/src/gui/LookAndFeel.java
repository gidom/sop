package gui;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;

public class LookAndFeel extends JFrame {

	private JPanel contentPane;
	private JFrame thisFrame = this;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LookAndFeel frame = new LookAndFeel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LookAndFeel() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new FlowLayout());
		setContentPane(contentPane);
		
		for (LookAndFeelInfo laf : UIManager.getInstalledLookAndFeels()) {
			final LookAndFeelInfo laf2 = laf;
			@SuppressWarnings("serial")
			JButton btn = new JButton(laf.getName()) {
				{
					addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								e.getSource().
								UIManager.setLookAndFeel(laf2.getClassName());
							} catch (Exception e1) {
								e1.printStackTrace();
							}
							SwingUtilities.updateComponentTreeUI(thisFrame);
							thisFrame.pack();
						}
					});
				}
			};
			contentPane.add(btn);
			
		}
		

	}

}