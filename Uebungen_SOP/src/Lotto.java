import java.util.HashSet;
import java.util.Set;

public class Lotto {
	Set<Integer> lottozahlen = new HashSet<Integer>();
	{
		for (int i = 1; i < 50; i++)
			lottozahlen.add(i);
	}

	public int getNumber() {
		while (true) {
			Integer i = new Integer(((int) (Math.random() * 49) + 1));
			if (lottozahlen.contains(i))
				return i;
		}
	}

	public void printSet() {
		for (Integer i : lottozahlen)
			System.out.println(i);
	}

	public static void main(String[] args) {
		new Lotto().printSet();
	}

}
