/**
 * 
 */
package logic;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author sigma
 *
 */
@XmlRootElement (name = "input")
public class InputExpression implements Expression {

    @XmlAttribute
    private String name;
    
    
    private boolean value;
    
    public InputExpression() {
	
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public InputExpression(String name, boolean value) {
	super();
	this.name = name;
	this.value = value;
    }
    
    public InputExpression(String name) {
 	this(name, false);
     }

    @XmlElement
    public boolean getValue() {
	return this.value;
    }
    
    public void setValue(boolean value) {
	this.value = value;
    }
    
    public void setFalse() {
	this.value = false;
    }
    
    public void setTrue() {
	value = true;
    }

    /* (non-Javadoc)
     * @see logic.Expression#isInput()
     */
    @Override
    public boolean isInput() {
	return true;
    }


}
