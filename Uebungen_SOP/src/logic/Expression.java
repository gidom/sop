/**
 * 
 */
package logic;

/**
 * @author sigma
 *
 */
public interface Expression {
    boolean getValue();
    
    boolean isInput();
}
