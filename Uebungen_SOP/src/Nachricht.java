public class Nachricht implements Comparable {
	private String inhalt;
	private int prio;

	/**
	 * @param inhalt
	 * @param prio
	 */
	public Nachricht(String inhalt, int prio) {
		super();
		this.inhalt = inhalt;
		this.prio = prio;
	}

	/**
	 * @return the inhalt
	 */
	public String getInhalt() {
		return inhalt;
	}

	/**
	 * @return the prio
	 */
	public int getPrio() {
		return prio;
	}

	@Override
	public int compareTo(Object anObject) {
		if (this == anObject) {
			return 0;
		}
		if (anObject instanceof Nachricht) {
			Nachricht anotherNachricht = ((Nachricht) anObject);
			return (anotherNachricht.prio > this.prio) ? -1
					: ((anotherNachricht.prio < this.prio) ? 1 : 0);
		}
		throw new ClassCastException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + prio;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nachricht other = (Nachricht) obj;
		if (prio != other.prio)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return inhalt + " " + prio;
	}
}
