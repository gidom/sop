/**
 * 
 */
package log2;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author sigma
 *
 */
@XmlRootElement
public class Input extends Expression {

    
    private String name;
    private boolean value;
    
    
    public Input() {
    }
    
    /**
     * @param name
     */
    public Input(String name) {
	this(name, false);
    }


    /**
     * @param name
     * @param value
     */
    public Input(String name, boolean value) {
	super();
	this.name = name;
	this.value = value;
    }

    
    /**
     * @return the name
     */
    @XmlAttribute
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * @param value the value to set
     */
    public void setVal(boolean value) {
        this.value = value;
    }

    /* (non-Javadoc)
     * @see log2.Expression#getValue()
     */
    @Override
    public boolean getValue() {
	return value;
    }

    /* (non-Javadoc)
     * @see log2.Expression#isInput()
     */
    @Override
    public boolean isInput() {
	return true;
    }

}
