/**
 * 
 */
package log2;

/**
 * @author sigma
 *
 */
public abstract class Expression {
    protected boolean isInput;
    
    public abstract boolean getValue();
    
    public abstract boolean isInput();
}
