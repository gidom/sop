package log2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 */

/**
 * @author sigma
 * 
 */
@XmlEnum(String.class)
public enum BooleanOperator {
    AND {
	public boolean link(boolean... a) {
	    boolean result = true;
	    for (boolean x : a)
		result &= x;
	    return result;
	}
    },
    OR {
	public boolean link(boolean... a) {
	    boolean result = false;
	    for (boolean x : a)
		result |= x;
	    return result;
	}
    },
    NAND {
	public boolean link(boolean... a) {
	    return !AND.link(a);
	}
    },
    NOR {
	public boolean link(boolean... a) {
	    return !OR.link(a);
	}
    },
    XOR {
	public boolean link(boolean... a) {
	    boolean result = false;
	    for (boolean x : a)
		result ^= x;
	    return result;
	}
    },
    XNOR {
	public boolean link(boolean... a) {
	    boolean result = false;
	    for (boolean x : a)
		result ^= x;
	    return !result;
	}
    },
    NOT;

    public boolean link(boolean... a) {
	if (a.length > 1)
	    throw new IllegalArgumentException();
	return !a[0];
    }

}
