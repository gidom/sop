/**
 * 
 */
package listen;

/**
 * @author sigma
 *
 */
public class ListeMain {
	public static void main(String[] args) {
		VerketteListe<String> vl = new VerketteListe<String>();
        System.out.print("Größe der Verketten Liste:" + vl.size());
		for (int i = 0; i < 10; i++)
			vl.add(Integer.valueOf(i).toString());
		vl.remove(4);
		for (String s:vl)
			System.out.println(s);
		System.out.println(vl.size());
		System.out.println(vl.get(8));
	}
}
