/**
 * 
 */
package listen;

/**
 * @author sigma
 * 
 */
public interface Liste<E> extends Iterable<E> {
	/**
	 * H�ngt das als Parameter �bergebene Objekt hinter dem letzten Element in
	 * die Liste ein.
	 * 
	 * @param elem
	 *            Element, dass in die Liste eingef�gt werden soll.
	 */
	void add(E elem);

	/**
	 * Entfernt das erste Vorkommen des als Parameter �bergebenen Objektes aus
	 * der Liste. Als R�ckgabewert wird das entfernte Objekt geliefert, um ein
	 * mit remove() konsistentes Verhalten zu erreichen. Ist das �bergebene
	 * Element nicht in der Liste enthalten, liefert remove() null zur�ck. Ist
	 * die Liste leer (gibt es also nichts zu entfernen), liefert remove()
	 * ebenfalls null zur�ck.
	 * 
	 * @param elem
	 *            Element, dass aus der Liste entfernt werden soll.
	 * @return Das entfernte Element, sonst null.
	 */
	E remove(E elem);

	/**
	 * Entfernt das an der als Parameter �bergebenen Stelle stehende Objekt aus
	 * der Liste und liefert es zur�ck. Wird ein Parameter �bergeben, der kein
	 * g�ltiger Index ist, d.h. kleiner als 0 oder gr��er gleich der Listenl�nge
	 * (�Out of bounds�), wird null zur�ckgegeben. remove() auf einer leeren
	 * Liste liefert ebenfalls null.
	 * 
	 * @param index
	 *            Index des Elements, dass aus der Liste entfernt werden soll.
	 * @return Das entfernte Element, sonst null.
	 */
	E remove(int index);

	/**
	 * Liefert die Stelle in der Liste, an der das als Parameter �bergebene
	 * Objekt das erste Mal vorkommt, oder -1 falls das Objekt nicht in der
	 * Liste vorkommt.
	 * 
	 * @param elem
	 *            Element nach dem in der Liste gesucht werden soll.
	 * @return Liefert den Index des ersten Vorkommens des Elements.
	 */
	int indexOf(E elem);

	/**
	 * Liefert das Objekt, das an der als Parameter �bergebenen Stelle in der
	 * Liste steht. Wird ein Parameter �bergeben, der kein g�ltiger Index ist,
	 * d.h. kleiner als 0 oder gr��er gleich der Listenl�nge (�Out of bounds�),
	 * wird null zur�ckgegeben. get() auf einer leeren Liste liefert ebenfalls
	 * null.
	 * 
	 * @param index
	 *            Index des zu suchenden Elements.
	 * @return Liefert das gefundene Element, sonst null.
	 */
	E get(int index);

	/**
	 * Liefert die Anzahl der Elemente in der Liste. Eine neu erzeugte Liste ist
	 * dabei leer und enth�lt dementsprechend 0 Elemente.
	 * 
	 * @return Die Gr��e der Liste.
	 */
	int size();

	/**
	 * Liefert true, wenn die Liste leer ist, und sonst false. Eine neu erzeugte
	 * Liste ist leer.
	 * 
	 * @return true falls die Liste nicht leer ist, sonst false.
	 */
	boolean isEmpty();

}
