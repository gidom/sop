/**
 * 
 */
package listen;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author dgierczak
 * 
 */
public class VerketteListe<E> implements Liste<E> {
	/**
	 * Number of elements in the list. 0 by default.
	 */
	private int size;

	/**
	 * Reference to the first elem in the list.
	 */
	private Entry<E> first;

	/**
	 * Stores the element in the underlying collection. Realized as inner class
	 * to prevent access from outside.
	 */
	private class Entry<ET> {
		/**
		 * Element to be stored in the list.
		 */
		ET elem;

		/**
		 * Reference to the next element.
		 */
		Entry<ET> next;

		/**
		 * Reference to the previous element.
		 */
		Entry<ET> prev;

		/**
		 * Creates a new element with a reference to the next and previous
		 * element.
		 * 
		 * @param elem
		 *            Element to be stored in the list.
		 * @param next
		 *            Reference to the next element.
		 * @param prev
		 *            Reference to the previous element.
		 */
		public Entry(ET elem, Entry<ET> next, Entry<ET> prev) {
			this.elem = elem;
			this.next = next;
			this.prev = prev;
		}

	}

	/**
	 * An iterator over a collection.
	 * 
	 * @param <ET>
	 *            the type of elements returned by this iterator
	 */
	private class ListenIterator<ET> implements Iterator<ET> {

		@SuppressWarnings("unchecked")
		Entry<ET> next = (Entry<ET>) first;
		Entry<ET> last;
		int offset;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			if (offset < size)
				return true;
			else
				return false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#next()
		 */
		@Override
		public ET next() {
			if (!hasNext())
				throw new NoSuchElementException();
			last = next;
			next = next.next;
			offset++;
			return next.prev.elem;

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#remove()
		 */
		@Override
		public void remove() {
            // Falls next() noch nicht aufgerufen, oder remove() zwei mal
            // aufgerufen wurde, folgt eine IllegalStateException
			if (last == null)
				throw new IllegalStateException("remove() vor next");
			// just one element
			if (size == 1)
				first = null;
			// first element to be removed
			if (last == first) {
				first = first.next;
				remove(last);
                last = null;
			} else {
				remove(last);
                last = null;
			}
			size--;
		}

		/**
		 * Removes entry from the list
		 * 
		 * @param e
		 *            entry to be removed
		 */
		private void remove(Entry<ET> e) {
			e.prev.next = e.next;
			e.next.prev = e.prev;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<E> iterator() {
		return new ListenIterator<E>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Listen.Liste#add(java.lang.Object)
	 */
	@Override
	public void add(E elem) {
		// empty list
		if (first == null) {
			first = new Entry<E>(elem, null, null);
			first.next = first;
			first.prev = first;
			// list not empty
		} else {
			first.prev.next = new Entry<E>(elem, first, first.prev);
			first.prev = first.prev.next;
		}
		size++;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Listen.Liste#remove(java.lang.Object)
	 */
	@Override
	public E remove(E elem) {
		E value;
		for (Iterator<E> it = iterator(); it.hasNext();) {
			value = it.next();
			if (value.equals(elem)) {
				it.remove();
				return value;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Listen.Liste#remove(int)
	 */
	@Override
	public E remove(int index) {
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException();

		E value = null;
		Iterator<E> it = iterator();
		for (int i = 0; i < index && it.hasNext(); i++) {
			value = it.next();
		}
		it.remove();
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Listen.Liste#indexOf(java.lang.Object)
	 */
	@Override
	public int indexOf(E elem) {
		int i = 0;
		for (Iterator<E> it = iterator(); it.hasNext(); i++)
			if (it.next().equals(elem))
				return i;
		return -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Listen.Liste#get(int)
	 */
	@Override
	public E get(int index) {
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException();

		E value = null;
		Iterator<E> it = iterator();
		for (int i = 0; i <= index && it.hasNext(); i++) {
			value = it.next();
		}
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Listen.Liste#size()
	 */
	@Override
	public int size() {
		return size;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Listen.Liste#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return size() == 0;
	}
    
        /*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator<E> it = iterator();
        for (int i = 0; i < size(); i++) {
            sb.append(it.next().toString());
            sb.append(i < (size() -1)  ? ", ": "]");
        }
        return sb.toString();
    }

}
