package designpattern.beilage;

import designpattern.Gericht;

public abstract class Beilage implements Gericht {
	protected int preis;
	protected String beschreibung;
	
	protected Gericht meinInneres;


	public Beilage(int preis, String beschreibung, Gericht meinInneres) {
		super();
		this.preis = preis;
		this.beschreibung = beschreibung;
		this.meinInneres = meinInneres;
	}

	
	@Override
	public int getPreis() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getBeschreibeung() {
		// TODO Auto-generated method stub
		return null;
	}

}