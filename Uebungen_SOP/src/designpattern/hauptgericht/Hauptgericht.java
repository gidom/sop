package designpattern.hauptgericht;

import designpattern.Gericht;

public abstract class Hauptgericht implements Gericht {
	protected int preis;
	protected String beschreibung;
	
	public Hauptgericht(int preis, String beschreibung) {
		super();
		this.preis = preis;
		this.beschreibung = beschreibung;
	}
	
	@Override
	public int getPreis() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getBeschreibeung() {
		// TODO Auto-generated method stub
		return null;
	}

}
