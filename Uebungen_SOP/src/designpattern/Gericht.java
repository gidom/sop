package designpattern;

public interface Gericht {
	int getPreis();
	String getBeschreibeung();
}
