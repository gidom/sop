/**
 * 
 */
package streams;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author dominik@gierczak.net
 * 
 */
public class HexOutputStream extends FilterOutputStream {

    /**
     * @param out
     */
    public HexOutputStream(OutputStream out) {
	super(out);
    }

    public void write(byte[] b) throws IOException {
	write(b, 0, b.length);
    }

    public void write(byte[] b, int off, int len) throws IOException {
	// vorzeitiges Ausl�sen der Exception zur Effizienzsteigerung
	if ((off | len | (b.length - (len + off)) | (off + len)) < 0)
	    throw new IndexOutOfBoundsException();

	StringBuilder sbHex = new StringBuilder(80), sbChar = new StringBuilder(), sb = new StringBuilder();
	for (int i = off, j = 1; i < len; i++, j++) {
	    sbChar.append(b[i] < 32 || b[i] > 126 ? "." : (char) b[i]);
	    sbHex.append(b[i] < 0 || b[i] > 15 ? String.format("%x ", b[i])
		    : String.format("0%x ", b[i]));

	    // Visuelle Strukturierung
	    if ((j % 16 == 0) || i == len - 1) {
		while (sbHex.length() % 54 != 0)
		    sbHex.append("   ");
		sb.append(sbHex).append(sbChar).append("\n");

		sbChar.delete(0, sbChar.length());
		sbHex.delete(0, sbHex.length());
	    }
	}
	out.write(sb.toString().getBytes(), 0, sb.length());
    }
}
