/**
 * 
 */
package streams;

import java.io.IOException;
import java.io.Reader;

/**
 * Liest eine Datei ein und ersetzt alle auftretenden Vokale durch ein
 * vorgegebenes Vokal. Wird kein Vokal vorgegeben, wirs 'a' angenommen.
 * 
 * @author dominik@gierczak.net
 * 
 */
public class VokalUmwandlungReader extends Reader {
	// nimmt das component vom typ Readable auf
	private Reader in;

	// das ersetzende Vokal
	private char vokal;

	/**
	 * Erzeugt einen VokalUmwandlungReader mit dem Readable r. Der Vokal wird
	 * standardmaessig festgesetzt.
	 * 
	 * @param r
	 *            Reader als Zeichenlieferant.
	 */
	public VokalUmwandlungReader(Reader r) {
		this(r, 'a');
	}

	/**
	 * Erzeugt einen VokalUmwandlungReader mit dem Readable r. Der Vokal wird
	 * als Parameter <code>vokal</code> uebergeben.
	 * 
	 * @param in
	 *            Reader als Zeichenlieferant.
	 * @param vokal
	 *            Das zu setztende Vokal.
	 */
	public VokalUmwandlungReader(Reader in, char vokal) {
		this.in = in;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.io.Reader#read(char[], int, int)
	 */
	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		// liest eine Zeichenkette aus dem Reader r mit dem Offset off und der
		// Laenge len. Treten hier Exceptions auf, werden sie weitergereicht.
		// Falls der Puffer cbuf leer ist, ist nichts mehr zu tun.
		int n;
		if ((n = in.read(cbuf, off, len)) == -1)
			return -1;
			
		// Umwandlung der Vokale mithilfe der regex
		String s = new String(cbuf);
		s = s.replaceAll("([aou]e|[AOU]E)", "a");
		s = s.replaceAll("([aeouiAEOUI])", "a");
		
		// der gewandelte String muss zurueck in den cbuff gespeichert werden.
		cbuf = s.toCharArray();
		
		// Rueckgabe der cbuf laenge
		return n;
	}


	@Override
	public boolean ready() throws IOException {
		throw new IOException("ready() not supported");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.io.Reader#close()
	 */
	@Override
	public void close() throws IOException {
		in.close();
	}
}
