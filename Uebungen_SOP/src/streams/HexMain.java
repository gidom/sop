/**
 * 
 */
package streams;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author sigma
 * 
 */
public class HexMain {
    public static void main(String[] args) throws Exception, IOException {
	if (args.length == 0)
	    throw new IndexOutOfBoundsException("Dateiname fehlt");

	File f = new File(args[0]);

	if (!f.exists())
	    throw new FileNotFoundException("Datei nicht gefunden");

	byte[] buff = new byte[8192];
	System.out.println(f.getAbsoluteFile());

	try (HexOutputStream bos = new HexOutputStream(
		new BufferedOutputStream(System.out));
		BufferedInputStream bis = new BufferedInputStream(
			new FileInputStream(f))) {
	    int n = bis.read(buff);
	    bos.write(buff, 0, n);
	}

    }

}
