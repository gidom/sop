package streams;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String in = "src/bible.txt";
		File fin = new File(in);
		File fout = new File(in.substring(0, in.length() - 4) + "_ausgabe.txt");

		try (BufferedReader br = new BufferedReader(new VokalUmwandlungReader(
				new FileReader(fin)));
				BufferedWriter bw = new BufferedWriter(new FileWriter(fout))) {

			String s;
			while ((s = br.readLine()) != null) {
				bw.write(s);
			}
			

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
