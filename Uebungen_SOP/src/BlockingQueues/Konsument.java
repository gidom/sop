package BlockingQueues;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.BlockingQueue;

/**
 * Konsument liest Sendungen aus der BlockingQueue.
 * 
 * @author hmueller
 */
public class Konsument implements Runnable {

    private Timer                    timer;
    private BlockingQueue<Nachricht> queue;
    private int                      delay;


    /**
     * Konstruktor
     * 
     * @param timer
     *            Timer-Instanz, die zu nutzen ist
     * @param queue
     *            Queue, aus der Nachrichten zu empfangen sind
     * @param delay
     *            Pause zwischen den Empfangsvorgängen
     */
    public Konsument( Timer timer, BlockingQueue<Nachricht> queue, int delay ) {
        this.timer = timer;
        this.queue = queue;
        this.delay = delay;
    }//eom


    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {

        try {
            Nachricht n;
            do {
                //Warten
                TimeUnit.SECONDS.sleep( delay );
               
                // Ausgabe "Ich fange an zu arbeiten"
                System.out.printf( "%05d -->K\n", this.timer.read() );

                // Lesen aus der Queue
                n = queue.take();

                // Ausgabe "Ich bin fertig mit Arbeiten"
                System.out.printf( "%05d K--> %s\n", this.timer.read(), n );

             
            } while( !n.getInhalt().equals( "ENDE" ) );
        }
        catch( InterruptedException ex ) {
            System.out.println( "Konsument wurde unterbrochen." );
        }

    }//eom

}//eoc
