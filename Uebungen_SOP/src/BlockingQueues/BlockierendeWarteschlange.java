package BlockingQueues;
import java.util.concurrent.*;

/**
 * "Haupt-Klasse" - erstellt einen Sendeplan, einen Produzenten, einen
 * Konsumenten und eine Warteschlange, �ber die Produzent und Konsument
 * kommunizieren k�nnen. Die eigentliche Kommunikation liegt dann in
 * Verwantwortung Konsument, Produzent und Warteschlange.
 * 
 * @author hmueller
 */
public class BlockierendeWarteschlange {

    // Timing Produzent: 0-1-2-3-[block]-5-28-29
    // Timing Konsument: 5-10-15-20-25-[block]-28-33

    public static void main( String[] args ) {

        // Sendeplan enth�lt alle Nachrichten samt Pausen
        final Sendung[] sendeplan =
                { new Sendung( new Nachricht( 5, "Nachricht 1" ), 1 ),
                        new Sendung( new Nachricht( 1, "Nachricht 2" ), 1 ),
                        new Sendung( new Nachricht( 5, "Nachricht 3" ), 1 ),
                        new Sendung( new Nachricht( 5, "Nachricht 4" ), 23 ),
                        new Sendung( new Nachricht( 1, "Nachricht 5" ), 1 ),
                        new Sendung( new Nachricht( 5, "ENDE" ), 1 ) };

        // BlockingQueue, die die Kommunikation zwischen Produzent und Konsument
        // steuert
        // In diese Queue passen 3 Objekte, dann ist sie voll und es muss
        // mindestens ein Objekt gelesen werden, bevor wieder "Platz ist"
        BlockingQueue<Nachricht> queue = new LinkedBlockingQueue<Nachricht>( 3 );

        // Timer-Instanz, die von allen anderen genutzt wird
        Timer timer = new Timer();

        // Produzent-Objekt, das die Sendungen in die Queue schreiben soll
        Produzent p = new Produzent( timer, queue, sendeplan );

        // Konsument-Objekt, das die Sendungen aus der
        Konsument k = new Konsument( timer, queue, 5 );

        // Thread f�r Produzent-Objekt
        Thread pt = new Thread( p );

        // Thread f�r Konsument-Objekt
        Thread kt = new Thread( k );

        // Timer starten
        timer.start();

        // Produzent starten
        pt.start();

        // Konsument starten
        kt.start();

        // Hier endet die main()-Methode, aber die Threads von Produzent und
        // Konsument laufen weiter, bis ihre Arbeit beendet ist. So lange l�uft
        // auch das Programm.

    }// eom main()

}// eoc
