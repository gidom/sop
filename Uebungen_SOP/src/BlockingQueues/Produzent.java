package BlockingQueues;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.BlockingQueue;

/**
 * Produzent erh�lt bei der Instanzierung einen Sendeplan, der die zu sendenden
 * Nachrichten in Form von Sendungen (d.h. mit Pausen) enth�lt. Er sendet eine
 * Nachricht/Sendung, und wartet dann die Pause ab, bevor er die n�chste Sendung
 * auf den Weg schickt.
 * 
 * @author hmueller
 */
public class Produzent implements Runnable {

    private Timer                    timer;
    private BlockingQueue<Nachricht> queue;
    private Sendung[]                sendeplan;


    /**
     * Konstruktor
     * @param timer Timer-Instanz, die zu nutzen ist
     * @param queue Platz f�r gesendete Sendungen
     * @param sendeplan hier kommen die zu sendenden Sendungen her
     */
    public Produzent( Timer timer, BlockingQueue<Nachricht> queue,
            Sendung[] sendeplan ) {
        this.timer = timer;
        this.queue = queue;
        this.sendeplan = sendeplan;
    }//eom


    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        try {

            /*
             * Sende alle Sendungen im Sendeplan 
             */
            for( Sendung sendung : sendeplan ) {
                
                Nachricht n = sendung.nachricht;
                
                //Ausgabe "Ich fange an zu arbeiten"
                System.out.printf( "%05d -->P\n", this.timer.read() );
                
                //Schreiben in die Queue
                queue.put( n );
                
                //Ausgabe "Ich bin fertig mit Arbeiten"
                System.out.printf( "%05d P--> %s\n", this.timer.read(), n );
                
                //Warten
                TimeUnit.SECONDS.sleep( sendung.pause );
            }

        }
        //Fehlerbehandlung
        catch( InterruptedException ex ) {
            System.out.println( "Produzent wurde unterbrochen." );
        }

    }//eom

}//eoc
