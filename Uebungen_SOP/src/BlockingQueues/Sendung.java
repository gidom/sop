package BlockingQueues;
/**
 * Zu sendende Nachricht, besteht aus der Nachricht selbst und der zugehörigen
 * Sendepause
 * 
 * @author hmueller
 */
public class Sendung {

    public final Nachricht nachricht;
    public final int       pause;


    /**
     * Konstruktor
     * 
     * @param nachricht
     *            die zu sendende Nachricht
     * @param pause
     *            die Pause nach der Sendung in s
     */
    public Sendung( Nachricht nachricht, int pause ) {
        this.nachricht = nachricht;
        this.pause = pause;
    }//eom

}//eoc
