package BlockingQueues;
/**
 * Nachrichten, die priorisierbar sind.
 *
 * @author hmueller
 */
public class Nachricht implements Comparable<Nachricht> {

   

    /**
     * Prioritšt der Nachricht 1...n, je kleiner, desto wichtiger
     */
    private int prio;
   

    /**
     * Inhalt der Nachricht
     */
    private String inhalt;
    
     /** Konstruktor
     * @param prio Prioritšt der Nachricht
     * @param inhalt Inhalt der Nachricht
     */
    public Nachricht( int prio, String inhalt ) {
        super();
        this.prio = prio;
        this.inhalt = inhalt;
    }//eom
    
    /**
     * @return the prio
     */
    public int getPrio() {
        return prio;
    }//eom
    
    /**
     * @return the inhalt
     */
    public String getInhalt() {
        return inhalt;
    }//eom
    
     /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo( Nachricht o ) {
       if( this.prio < o.prio ){
           return -1;
       }
       
       if( this.prio > o.prio ){
           return +1;
       }
        return 0;
    }//eom

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.inhalt + " (" + this.prio + ")";
    }//eom
    
}//eoc
