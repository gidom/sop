package BlockingQueues;
/**
 * Hilfsklasse f�rs Timing
 *
 * @author hmueller
 */
public class Timer {
    
    private long basis;
    private boolean isRunning = false;
    
    /**
     * Startet diesen Timer
     */
    public void start(){
        this.basis = System.currentTimeMillis();
        this.isRunning = true;
    }//eom
    
    /**
     * Liest den Timer
     * @return die abgelaufene Zeit in ms
     * @throws IllegalStateException Timer lesen ohne vorher zu starten -> bl�d
     */
    public long read() throws IllegalStateException{
        if( this.isRunning ){
            long current = System.currentTimeMillis();
            return current - this.basis;
        }
        //Fehlerbehandlung
        throw new IllegalStateException("Timer is not running");
    }//eom
    
    /**
     * H�lt den Timer an
     * @return Laufzeit in ms
     */
    public long stop(){
        this.isRunning = false;
        return this.read();
    }//eom
    
    /**
     * Running-Status des Timers
     * @return
     */
    public boolean isRunning(){
        return this.isRunning;
    }//eom

}//eoc
