package exceptions.bankkonto.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import exceptions.bankkonto.BankKonto;
import exceptions.bankkonto.TransaktionsException;

public class BankKontoTest {

	BankKonto bk;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Before
	public void initBeforeTest() {
		bk = new BankKonto();
	}

	@Test
	public void testEinzahlen() throws TransaktionsException {
		bk.einzahlen(1);
		assertEquals(1, bk.getSaldo(), 0.001);
	}

	@Test
	public void testEinzahlenNegativ() throws TransaktionsException {
		thrown.expect(TransaktionsException.class);
		thrown.expectMessage("Betrag negativ");
		bk.einzahlen(-1);
	}

	@Test
	public void testAuszahlen() throws TransaktionsException {
		bk.einzahlen(1);
		bk.auszahlen(1);
		assertEquals(0, bk.getSaldo(), 0.001);
	}

	@Test
	public void testAuszahlenSaldo() throws TransaktionsException {
		thrown.expect(TransaktionsException.class);
		thrown.expectMessage("zu wenig Saldo");
		bk.auszahlen(1);
	}

	@Test
	public void testAuszahlenNegativ() throws TransaktionsException {
		thrown.expect(TransaktionsException.class);
		thrown.expectMessage("Betrag negativ");
		bk.auszahlen(-1);
	}
}
