package exceptions.bankkonto;

public class TransaktionsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5653835210340563526L;

	/**
	 * 
	 */
	public TransaktionsException() {
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public TransaktionsException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public TransaktionsException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public TransaktionsException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public TransaktionsException(Throwable cause) {
		super(cause);
	}

	

	
}
