package exceptions.bankkonto;


public class BankKonto {
	private double saldo;

	public void einzahlen(double betrag) throws TransaktionsException {
		// abrupte Terminierung falls betrag negativ
		if (betrag < 0)
			throw new TransaktionsException("Betrag negativ");
		else
			saldo += betrag;
	}

	public void auszahlen(double betrag) throws TransaktionsException {
		// abrupte Terminierung falls betrag negativ
		if (betrag < 0)
			throw new TransaktionsException("Betrag negativ");
		
		//abrupte Terminierung falls betrag > Saldo
		if (saldo < betrag )
			throw new TransaktionsException("zu wenig Saldo");
		
		saldo -= betrag;
	}

	public double getSaldo() {
		return saldo;
	}
}
