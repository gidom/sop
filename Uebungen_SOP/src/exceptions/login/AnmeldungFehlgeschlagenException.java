package exceptions.login;

public class AnmeldungFehlgeschlagenException extends Exception {

	public AnmeldungFehlgeschlagenException(String message) {
		super(message);
	}
}
