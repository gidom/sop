package exceptions.login;

public class KeineBerechtigungException extends Exception{

	public KeineBerechtigungException(String message) {
		super(message);
	}

}
