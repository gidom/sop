package exceptions.login;

public class Login {
	private boolean angemeldet;
	private String username;
	private String password;

	// ## Konstruktoren ##
	public Login(String username, String password) {
		this.username = username;
		this.password = password;
	}

	// ## Methoden ##
	public void anmelden(String benutzer, String passwd)
			throws AnmeldungFehlgeschlagenException {
		
		// Falls der User bereits angemeldet ist, soll dieser sich nicht nochmal
		// anmelden d�rfen
		if (angemeldet)
			throw new AnmeldungFehlgeschlagenException(
					"User bereits angemeldet");

		if (benutzer.equalsIgnoreCase(username) && passwd.equals(password))
			angemeldet = true;
		else
			// Falls Benutzerdaten nicht korrekt, erfolgt eine Fehlermeldung
			throw new AnmeldungFehlgeschlagenException(
					"Anmeldung Fehlgeschlagen");
	}

	public void abmelden() throws KeineBerechtigungException {
		if (!angemeldet)
			// Exception, falls Nutzer nicht angemeldet
			throw new KeineBerechtigungException("Keine Berechtigung");
		angemeldet = false;
	}

	/*
	 * Wer angemeldet ist darf arbeiten
	 */
	public void arbeiten() throws KeineBerechtigungException {
		if (!angemeldet)
			// Exception, falls Nutzer nicht angemeldet
			throw new KeineBerechtigungException("Keine Berechtigung");
		System.out.print("isch maloche");
	}

}
