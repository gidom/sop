package exceptions.login.test;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import exceptions.login.AnmeldungFehlgeschlagenException;
import exceptions.login.KeineBerechtigungException;
import exceptions.login.Login;

public class LoginTest {
	Login login;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	/*
	 * init f�r die Umleitung der Standardausgabe auf outContent
	 */
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

	@Before
	public void setUpStreams() {
	    System.setOut(new PrintStream(outContent));
	}

	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	}

	
	
	@Before
	public void setUpLogin() {
		// Login Instanz als Basis f�r die Tests
		login = new Login("hans", "wurst");
	}

	@Test
	/*
	 *  Anmelden_Testfall_1 Benutzername korrekt, Passwort falsch
	 */
	public void testAnmelden1() throws AnmeldungFehlgeschlagenException {
		thrown.expect(AnmeldungFehlgeschlagenException.class);
		thrown.expectMessage("Anmeldung Fehlgeschlagen");
		login.anmelden("hans", "testpassword");

	}

	@Test
	/*
	 * Anmelden_Testfall_2 Benutzername falsch, Passwort korrekt
	 */
	public void testAnmelden2() throws AnmeldungFehlgeschlagenException {
		thrown.expect(AnmeldungFehlgeschlagenException.class);
		thrown.expectMessage("Anmeldung Fehlgeschlagen");
		login.anmelden("Karl", "wurst");
	}

	@Test
	/*
	 * Anmelden_Testfall_3 Benutzer bereits angemeldet und versucht sich neu
	 * anzumelden gleichzeitig wird getestet, ob die anmeldung funktioniert
	 */
	public void testAnmelden3() throws AnmeldungFehlgeschlagenException {
		thrown.expect(AnmeldungFehlgeschlagenException.class);
		thrown.expectMessage("User bereits angemeldet");
		login.anmelden("hans", "wurst");
		login.anmelden("hans", "wurst");
	}

	@Test
	/* 
	 * Abmelden_Testfall_1
	 * Benutzer ist nicht angemeldet
	 */
	public void testAbmelden1() throws KeineBerechtigungException{
		thrown.expect(KeineBerechtigungException.class);
		thrown.expectMessage("Keine Berechtigung");
		login.abmelden();
	}
	
	@Test
	/* 
	 * Abmelden_Testfall_2
	 * Benutzer ist angemeldet und wird erfolgreich abgemeldet
	 */
	public void testAbmelden2() throws KeineBerechtigungException, AnmeldungFehlgeschlagenException{
		thrown.expect(KeineBerechtigungException.class);
		thrown.expectMessage("Keine Berechtigung");
		
		// erfolgreich anmelden
		login.anmelden("hans", "wurst");
		login.abmelden();
		login.arbeiten();
	}
	
	

	@Test
	/*
	 * Arbeiten_Testfall_1
	 * Benutzer nicht angemeldet und versucht methode arbeiten() aufzurufen
	 */
	public void testArbeiten1() throws KeineBerechtigungException{
		thrown.expect(KeineBerechtigungException.class);
		thrown.expectMessage("Keine Berechtigung");
		login.arbeiten();
	}
	
	@Test
	/*
	 * Arbeiten_Testfall_2
	 * Nutzer angemeldet
	 */
	public void testArbeiten2() throws AnmeldungFehlgeschlagenException, KeineBerechtigungException{
		login.anmelden("hans", "wurst");
		login.arbeiten();
		assertEquals("isch maloche", outContent.toString());
	}

}
