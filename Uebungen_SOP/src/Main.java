import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		Queue<Nachricht> ws = new LinkedList<Nachricht>();

		ws.offer(new Nachricht("Feuer1", 1));
		ws.offer(new Nachricht("Feuer3", 3));
		ws.offer(new Nachricht("Feuer2", 2));

		Collections.sort((LinkedList<Nachricht>) ws);

		while (!ws.isEmpty())
			System.out.println(ws.remove());
	}
}
