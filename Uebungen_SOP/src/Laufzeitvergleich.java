import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author dgierczak
 * 
 */
public class Laufzeitvergleich {

	private int getListFromUser() {
		System.out.print("ArrayList (1) | LinkedList (2)\n :");
		return new Scanner(System.in).nextInt();
	}

	private List<Integer> fill(List<Integer> list) {
		for (int i = 0; i < 5000; i++)
			list.add((int) (Math.random() * 10000));
		return list;
	}

	private void read(List<Integer> list) {
		for (int i = 0; i < 10000; i++)
			list.get(list.size() / 2);
	}

	public void start() {
		List<Integer> list;

		switch (getListFromUser()) {
		case 1:
			list = new ArrayList<Integer>();
			break;
		default:
			list = new LinkedList<Integer>();
		}
		// Zeitmessung
		long t1 = System.currentTimeMillis();
		fill(list);
		long t2 = System.currentTimeMillis();
		read(list);
		long t3 = System.currentTimeMillis();

		System.out.println("Zeit beim schreiben in ms: " + (t2 - t1));
		System.out.println("Zeit beim lesen in ms: " + (t3 - t2));

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Laufzeitvergleich().start();
	}

}
