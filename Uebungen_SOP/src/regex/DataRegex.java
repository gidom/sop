/**
 * 
 */
package regex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sigma
 * 
 */
public class DataRegex {
	/* ############### member ############## */
	private Map<String, String> recordMap;
	private List<Map<String, String>> recordList;
	private Pattern kvPattern, codePattern;

	// key is the token before the colon
	private final String KEY = "(?<key>^.*)(?=:)";

	// value is the token after the colon
	private final String VALUE = ":\\s*(?<value>.*\\S)$";

	// comment is introduced w/ a number sign '#'
	// private final String COMMENT_REGEX =
	// "((?<=#\\s{0,80})(?<comment>\\S.*\\S))";
	private final String CODE_REGEX = "^(?=[^\\r\\n#]+)(?=\\s*(?<code>.*\\S(?=\\s*#)|.*\\S))";

	/* ############### constructor ############## */
	public DataRegex() {
		kvPattern = Pattern.compile(KEY + VALUE);
		codePattern = Pattern.compile(CODE_REGEX);
		recordList = new ArrayList<Map<String, String>>();
	}

	/* ############### methods ############## */
	public void loadData(File file) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			for (String s = br.readLine(); s != null; s = br.readLine()) {
				// comments are ignored
				Matcher m = codePattern.matcher(s);
				if (m.find()) {
					extractRecords(m.group("code"));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void extractRecords(String s) {
		// delimiter marks the end of one record
		String borderTag = "^---$";
		Matcher m = Pattern.compile(borderTag).matcher(s);

		if (m.find()) {
			if (recordMap != null) {
				recordList.add(recordMap);
			}
			recordMap = new HashMap<String, String>();
		} else {
			m = kvPattern.matcher(s);
			// finds key and value and puts to map
			if (m.find()) {
				recordMap.put(m.group("key"), m.group("value"));
			}
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Map<String, String> m : recordList) {
			sb.append(m.toString() + System.lineSeparator());
		}
		return sb.toString();
	}
}
